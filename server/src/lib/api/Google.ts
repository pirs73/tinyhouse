import { google, people_v1 } from 'googleapis';
// import { Client } from '@googlemaps/google-maps-services-js';

const auth = new google.auth.OAuth2(
  process.env.G_CLIENT_ID,
  process.env.G_CLIENT_SECRET,
  `${process.env.PUBLIC_URL}/login`
);

// const client = new Client({});

// client
//   .elevation({
//     params: {
//       locations: [{ lat: 45, lng: -110 }],
//       key: process.env.G_GEOCODE_KEY || '',
//     },
//     timeout: 1000, // milliseconds
//   })
//   .then((r) => {
//     return r.data.results[0].elevation;
//   })
//   .catch((e) => {
//     console.log(e);
//   });

export const Google = {
  authUrl: auth.generateAuthUrl({
    access_type: 'online',
    scope: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ],
  }),
  logIn: async (code: string): Promise<{ user: people_v1.Schema$Person }> => {
    const { tokens } = await auth.getToken(code);

    auth.setCredentials(tokens);

    const { data } = await google.people({ version: 'v1', auth }).people.get({
      resourceName: 'people/me',
      personFields: 'emailAddresses,names,photos',
    });

    return { user: data };
  },

  // geocode: async (address: string | undefined) => {
  //   const res = await client.geocode({{ address } });

  //   if (res.status < 200 || res.status > 299) {
  //   }
  // },
};
